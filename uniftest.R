# SIGFRACTION=0.000
# jackdatafolder = '/g/furlong/project/24_TSSCAGE/analysis/FULLDATA_QTL6'
# jackdatafolderfiles = jackdatafolder %>%listFiles
# jackdatafolderfiles
# load('/g/furlong/Harnett/TSS_CAGE_myfolder/data/objects/allcage.object.R')
# timepoints=w('24h 68h 1012h')


# message('reading jacksl files')
# jackdatafolderdf = data.frame(	file=jackdatafolderfiles)%>%tbl_df
# jackdatafolderdf%<>%cbind(
# 	jackdatafolderdf$file%>%basename%>%str_match(perl('(chr\\w+)_(\\-?\\d+)_(\\d+)_?(\\+|-|RNAdata)'))%>%e(,-1)%>%
# 		set_colnames(w('seqnames start end strand'))%>%
# 		as.data.frame%>%
# 		mutate(strand = strand%>%str_replace('RNAdata','*'))
# )
# jackdatafolderdf%<>%mutate(window = paste0(seqnames,'_',start,'_',end))
# #check the regex worked
# jackdatafolderdf%>%lapply(expectVals)
# stopifnot(names(jackdatafolderdf)== c("file","seqnames","start","end","strand"))
# jackdatafolderdf$isRNA = jackdatafolderdf$strand=='*'


# #so it looks like these objects go sense strand, other strand
# #depending on the strand of the object

# require(XML)
# plotfolder = '/g/furlong/Harnett/TSS_CAGE_myfolder/analysis/uniffiltertestplots/'
# windowstotest=
# c(
# 	pos='chr2L_18695382_18696405',
# 	pos='chr3R_27443203_27444226',
# 	pos='chr2R_3373292_3374315',
# 	pos='chr2R_14858097_14859120',
# 	pos='chr3L_18102093_18103116',
# 	pos='chr2L_2144642_2145665',
# 	pos='chr2L_3461406_3462429',
# 	neg='chr3R_21129353_21130376',
# 	neg='chr3R_11089420_11090443',
# 	neg='chr2R_9122979_9124002',
# 	neg='chr3R_1584147_1585170'
# )
# cagtopqtlfile='/g/furlong/Harnett/TSS_CAGE_myfolder/analysis/cageTopSnpTable.txt'
# cageTopSnpTable=fread(cagtopqtlfile)

# windowstotest = windowstotest%>%c(cageTopSnpTable$Window%>%sample(20)%>%setNames(rep('neg',length(.))))

##SOURCEON
strands = c('+','-')
splitcolinds = split(seq_along(posvb),rep(1:2,each=length(posv)/2))%>%setNames(strands)
uniftestlist = expand.grid(strand=strands,timepoint=timepoints,i=snpinds,window=windowstotest)
changedirvect=list()

for(iwindow in windowstotest){
	message('window ':iwindow)
	#get the genotype table
	gtable = failwith(readHTMLTable,default=NULL)('http://furlonglab.embl.de/xchange/degner/APR15_3PC_FreeForm/':iwindow:'/':iwindow:'SequenceInfo.html')
	if(is.null(gtable)){changedirvect[[iwindow]]=NULL;next}
	#figure otu which lines should be pos and which should be neg
	gtable[[2]]%>%dim
	gtable[[2]]%<>%.[,-c(1:9)]
	# gtable[[2]][]=gtable[[2]][]!='0/0'
	gtable[[2]]%<>%t
	# gtable[[2]]%>%str
	snpinds=seq_len(ncol((gtable[[2]])))
	poslist=lapply(snpinds,.%>%{rownames(gtable[[2]])[ gtable[[2]] [TRUE,.,drop=TRUE]=='1/1' ]%>%unique%>%str_extract('\\d+')%>%as.numeric })
	neglist=lapply(snpinds,.%>%{rownames(gtable[[2]])[ gtable[[2]] [TRUE,.,drop=TRUE]=='0/0' ]%>%unique%>%str_extract('\\d+')%>%as.numeric  })
	#get a single object, parse the row names of the pheno object
	f=jackdatafolderdf%>%filter(file%>%str_detect(iwindow),strand!='*')%$%file
	object = new.env()%>%{load(f,.);.}%>%as.list
	#convert the line numbers to jacks inds
	object[['inds']]%<>%as.numeric
	poslist%<>%lapply( .%>%match(.,object[['inds']])%>%vfilt(Negate(is.na)))
	neglist%<>%lapply( .%>%match(.,object[['inds']])%>%vfilt(Negate(is.na)))
	#so our poslist has teh alternate allele
	for(i in snpinds){
		refallemaj = ifelse(length(poslist[[i]])<length(neglist[[i]]),"Major",'Minor')
		altallemaj = ifelse(length(poslist[[i]])>length(neglist[[i]]),"Major",'Minor')

		#get all the rows from the genotypes with the snp
		posmat = object[['phenoTbyInd_NORM']]%>%lapply(.%>%{.[poslist[[i]],,drop=FALSE]})%>%do.call(rbind,.)
		negmat = object[['phenoTbyInd_NORM']]%>%lapply(.%>%{.[neglist[[i]],,drop=FALSE]})%>%do.call(rbind,.)

		#discrete
		posmatcount = object[['phenoTbyInd']]%>%lapply(.%>%{.[poslist[[i]],,drop=FALSE]})%>%do.call(rbind,.)
		negmatcount = object[['phenoTbyInd']]%>%lapply(.%>%{.[neglist[[i]],,drop=FALSE]})%>%do.call(rbind,.)

		posvb=posmat%>%colMeans
		negvb=negmat%>%colMeans
		
		# sigbases = rbind(posmat,negmat)%>%colSums%>%{.>(sum(.)*SIGFRACTION)}
		sigbases = rbind(posmat,negmat)%>%colSums%>%{.>(sum(.)*SIGFRACTION)}

		# sigbases = ((posmatcount)%>%colSums%>%gt(10)) & ((negmatcount)%>%colSums%>%gt(10))


		strandinds=splitcolinds[[1]]

		for(strand in strands){
			strandinds = splitcolinds[[strand]]
			sigbasesstrand = sigbases[strandinds]

			posv=posvb[strandinds]
			negv=negvb[strandinds]
			changedirvect[[iwindow]][[as.character(i)]][[strand]]=(posv-negv)[sigbasesstrand]
		}
	}
}


changedirvect%>%lapply(.%>%lapply(.%>%lapply(.%>%{max(gt(.,0)%>%mean,lt(.,0)%>%mean) }  ) )%>%unlist%>%max(na.rm=TRUE))
changedirvect%>%lapply(.%>%lapply(.%>%lapply(.%>%{max(gt(.,0)%>%mean,lt(.,0)%>%mean) }  ) )%>%unlist%>%max(na.rm=TRUE))
changedirvect%>%lapply(.%>%lapply(.%>%lapply(.%>%{max(gt(.,0)%>%mean,lt(.,0)%>%mean) }  ) )%>%unlist%>%max(na.rm=TRUE))

testvals = changedirvect%>%lapply(.%>%lapply(.%>%.[['+']]%>%{max(gt(.,0)%>%mean,lt(.,0)%>%mean) })%>%unlist%>%max(na.rm=TRUE))

testvals%>%setNames(windowstotest%>%names)%>%list2tab%>%setnames(w('set value'))%>%group_by(name)%>%
	{
		qplot(data=.,x=set,color=set,fill=set,y=value,adjust=0.01,geom='point',position= position_jitter(width=0.1))+
	ggtitle('mean number of bases wth counts \n changing in dominant direction')
}


stop()

######Now with SVD instad

for(window in windowstotest){
	#get the genotype table
	gtable = readHTMLTable('http://furlonglab.embl.de/xchange/degner/APR15_3PC_FreeForm/':window:'/':window:'SequenceInfo.html')
	#figure otu which lines should be pos and which should be neg
	gtable[[2]]%>%dim
	gtable[[2]]%<>%.[,-c(1:9)]
	# gtable[[2]][]=gtable[[2]][]!='0/0'
	gtable[[2]]%<>%t
	gtable[[2]]%>%str
	snpinds=seq_len(ncol((gtable[[2]])))
	poslist=lapply(snpinds,.%>%{rownames(gtable[[2]])[ gtable[[2]] [TRUE,.,drop=TRUE]=='1/1' ]%>%unique%>%str_extract('\\d+')%>%as.numeric })
	neglist=lapply(snpinds,.%>%{rownames(gtable[[2]])[ gtable[[2]] [TRUE,.,drop=TRUE]=='0/0' ]%>%unique%>%str_extract('\\d+')%>%as.numeric  })
	#get a single object, parse the row names of the pheno object
	f=jackdatafolderdf%>%filter(file%>%str_detect(window),strand!='*')%$%file
	object = new.env()%>%{load(f,.);.}%>%as.list
	#convert the line numbers to jacks inds
	object[['inds']]%<>%as.numeric
	poslist%<>%lapply( .%>%match(.,object[['inds']])%>%vfilt(Negate(is.na)))
	neglist%<>%lapply( .%>%match(.,object[['inds']])%>%vfilt(Negate(is.na)))

	refallemaj = rl('http://furlonglab.embl.de/xchange/degner/APR15_3PC_FreeForm/':window:'/':window:'SequenceInfo.html',1)%>%str_extract(perl('(?=Reference allele is )\\w+'))
	altallemaj = ifelse(refallemaj=='Reference','Minor','Reference')

	#so our poslist has teh alternate allele

	for(i in snpinds){

		#get all the rows from the genotypes with the snp
		posmat = object[['phenoTbyInd_NORM']]%>%lapply(.%>%{.[poslist[[i]],,drop=FALSE]})%>%do.call(rbind,.)
		negmat = object[['phenoTbyInd_NORM']]%>%lapply(.%>%{.[neglist[[i]],,drop=FALSE]})%>%do.call(rbind,.)
		allmat=rbind(posmat,negmat)
		genovect = c((length(poslist[[i]])*3)%>%rep(1,,.), (length(neglist[[i]])*3)%>%rep(0,,.))
		require(sva)
		#get decomposition of matrix
		svds = svd(allmat)
		#find the svd which best correlates with the genotype
		sapply(1:10,function(x){cor(svds$u[,x],genovect)})


			
		cor(ob$v[,1],effectvect)


		posvb=posmat%>%colMeans
		negvb=negmat%>%colMeans
		
		sigbases = rbind(posmat,negmat)%>%colSums%>%{.>(sum(.)*SIGFRACTION)}
		

		strandinds=splitcolinds[[1]]

		for(strand in strands){
			strandinds = splitcolinds[[strand]]
			sigbasesstrand = sigbases[strandinds]

			posv=posvb[strandinds]
			negv=negvb[strandinds]
			changedirvect[[window]][[as.character(i)]][[strand]]=(posv-negv)[sigbasesstrand]
		}
	}
}


testvals%>%setNames(windowstotest%>%names)%>%list2tab%>%setnames(w('set value'))%>%group_by(name)%>%
	{qplot(data=.,x=set,fill=set,y=value,color=set,position='jitter',geom='point')+ggtitle('mean number of bases changing in dominant direction')}




##SOURCEOFF
				both = (posv!=0) & (negv!=0)
				posvs=posv[both]
				negvs=negv[both]
				tot=sum(posv)
				ntot=sum(negv)
				nsites=length(posvs)

				posvs/tot - negvs/ntot

				#difference between predicted
				diffv=negvs-qbetabinom_ab(rep(1-(1e-5),nsites),size=rep(ntot,nsites),posvs+1,tot-posvs+1 )
				diffv=diffv<0
				ggdf=
					list(alt=posv,ref=negv)%>%
					setNames(c(altallemaj,refallemaj))%>%
					stack%>%
					set_colnames(w('count genotype'))%>%
					mutate(pos=rep(seq_along(posv),2))%>%
					group_by(genotype)%>%mutate(countfrac=count/sum(count))%>%
					mutate(countfrac=ifelse(genotype==refallemaj,-countfrac,countfrac))%>%
					# filter(pos%>%between(1,100))
					identity
				ggdf%<>%filter(count!=0)
				maxval=ggdf%$%countfrac%>%abs%>%max
					# ggdf%>%qplot(data=.,y=countfrac,x=pos,fill=genotype,geom='point',stat='identity')
				p=ggdf%>%qplot(data=.,ylab='fraction of reads in site',y=countfrac,x=pos,color=genotype,geom='point',stat='identity',ylim=c(-maxval,maxval))
				p=p+ggtitle('uniformEffectQTL:\n':window:'_snp':i:'_tp':tp:'_strand_':sense)
				sense= ifelse(strandinds[1]==1,'sense','antisense')
				plotname=plotfolder:window:'_snp':i:'_tp':tp:'_strand_':sense:'.pdf'
				ggsave(plot=p,file=plotname)
			}
	}
}

ggdf=
	list(alt=posv,ref=negv)%>%
	setNames(c(altallemaj,refallemaj))%>%
	stack%>%
	set_colnames(w('count genotype'))%>%
	mutate(pos=rep(seq_along(posv),2))%>%
	group_by(genotype)%>%mutate(countfrac=count/sum(count))%>%
	mutate(countfrac=ifelse(genotype==refallemaj,-countfrac,countfrac))%>%
	# filter(pos%>%between(1,100))
	identity
ggdf%<>%filter(count!=0)
maxval=ggdf%$%count%>%abs%>%max
	# ggdf%>%qplot(data=.,y=countfrac,x=pos,fill=genotype,geom='point',stat='identity')
p=ggdf%>%qplot(data=.,ylab='fraction of reads in site',group=genotype,y=count+1,x=pos,color=genotype,geom='line',stat='identity',ylim=c(0,maxval),log='y')
p+scale_y_log10()

file2laptop(plotfolder)
plotfolder%>%listFiles

#testing principal component analysis
coln=100
rown=400
mu = 300
effmu=50
tmpmat = matrix(rnorm(coln*rown,mu),ncol=coln)
affected=rbinom(rown,1,0.5)%>%as.logical
naffected=sum(affected)
effectvect = rep(0,coln)
neffectedcols = rbinom(coln,1,0.5)%>%sum
effectvect[sample(1:coln,neffectedcols)] = rnorm(neffectedcols,effmu)
scores = rep(0,rown)
scores[affected] = rnorm(naffected,2) 

tmpmat=tmpmat+sapply(effectvect,'*',scores)

tmpmat%>%dim

ob=princomp(tmpmat)
ob%>%str
cor(ob$scores[,1],effectvect)
cor(ob$scores[,1],scores)


ob=svd(tmpmat)
ob%>%str
cor(ob$v[,1],effectvect)
cor(ob$u[,1],scores)


cor(ob$scores[3,],effectvect)
cor(ob$scores[4,],effectvect)
cor(ob$scores[,1],effectvect)



#testing principal component analysis
coln=100
rown=400
mu = 300
effmu=50
tmpmat = matrix(rnorm(coln*rown,mu),ncol=coln)
affected=c(rep(FALSE,rown/2),rep(TRUE,rown/2))
naffected=sum(affected)
effectvect = rep(0,coln)
neffectedcols = rbinom(coln,1,0.5)%>%sum
effectvect[sample(1:coln,neffectedcols)] = rnorm(neffectedcols,effmu)
scores = rep(0,rown)
scores[affected] = rnorm(naffected,2) 

tmpmat=tmpmat+sapply(effectvect,'*',scores)

#now with sva
mod = model.matrix(~factor(affected),data=as.data.frame(tmpmat))
mod0 = model.matrix(~1,data=as.data.frame(tmpmat))
ob=sva(tmpmat%>%t,mod,mod0)
ob=sva(tmpmat%>%t,mod,mod0)
?sva
ob%>%str
cor(ob$sv,effectvect)
cor(ob$sv,scores)
cor(ob$pprob.b,effectvect)


sva


tmpmat%>%dim
#with principle componenet analysis
ob=princomp(tmpmat)
ob%>%str
cor(ob$scores[,1],effectvect)
cor(ob$scores[,1],scores)

#with svd
ob=svd(tmpmat)
ob%>%str
cor(ob$v[,1],effectvect)
cor(ob$u[,1],scores)


cor(ob$scores[3,],effectvect)
cor(ob$scores[4,],effectvect)
cor(ob$scores[,1],effectvect)