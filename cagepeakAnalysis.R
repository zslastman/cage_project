allsmoothpeaks  	=readRDS(file='/g/furlong/Harnett/Phospho_polII_myfolder/data/objects/allsmoothpeaks.object.R')
allsmoothinternal 	=readRDS(file='/g/furlong/Harnett/Phospho_polII_myfolder/data/objects/allsmoothinternal.object.R')
allsmoothmain 		=readRDS(file='/g/furlong/Harnett/Phospho_polII_myfolder/data/objects/allsmoothmain.object.R')
tssregions = genes%>%resize(1,'start')%>%expand_ranges_disjoint(expandtp=FALSE,2e3-250)
tssregions = tssregions[,NULL]
tssregions = c(tssregions,transcripts%>%resize(1e3,'start')%>%.[,NULL])


#how many QTLs overlap internal, external cage signal
setkey(cageQTLTable,seqnames,gene_name,start,end)
allsmoothmain%<>%GR2DT%>%setkey(seqnames,gene_name,start,end)
allsmoothinternal%<>%GR2DT%>%setkey(seqnames,gene_name,start,end)
#ovelrap, accounting for gene name
cageQTLTable$overlapsMain = cageQTLTable%>%foverlapsAny(allsmoothmain)
cageQTLTable$overlapsInternal = cageQTLTable%>%foverlapsAny(allsmoothinternal) & (! cageQTLTable$overlapsMain)



ggdf=list(
	strandswitch = cageQTLTable%>%filter(StrandSwitch)%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal)),
	Redistribution = cageQTLTable%>%filter(Redistribution)%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal)),
	RNAconc = cageQTLTable%>%filter(validRNA & RNA)%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal)),
	RNANonConc = cageQTLTable%>%filter(validRNA & (!RNA))%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal)),
	Directional = cageQTLTable%>%filter(Directional) %>% summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal))
)%>%stackdfs('set')%>%gather(peaktype,number,Main,internal)

ggdf%<>%group_by(set)%>%mutate(frac = number/sum(number))
ggdf%>%{qplot(data=.,x=set,fill=peaktype,y=number,geom='bar',stat='identity',position='dodge')}%>%ggsave(plot=.,'/g/furlong/Harnett/TSS_CAGE_myfolder/feature_analysis/eqtl_loc_peaktype_prop.pdf')
qplot(data=ggdf,x=set,fill=peaktype,y=frac,geom='bar',stat='identity',position='dodge')%>%ggsave(plot=.,'/g/furlong/Harnett/TSS_CAGE_myfolder/feature_analysis/eqtl_loc_peaktype_prop_frac.pdf')


#Now we want to know how many eQTLs we lose in various filters...for some reason
cageQTLTable$overlapsAnyPeakNoFilt=cageQTLTable%>%DT2GR%>%overlapsAny(allsmoothpeaks[['tp68hembryo']]$nofilt%>%subset(score>0))
cageQTLTable$overlapsAnyPeak=cageQTLTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$all%>%subset(score>0))
cageQTLTable$overlaps_score_50_peak=cageQTLTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$all%>%subset(score>50))
cageQTLTable$overlaps_gene_peak=cageQTLTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$gene)
cageQTLTable$overlaps_TSS_peak=cageQTLTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$gene%>%subsetByOverlaps(tssregions))
# cageQTLTable$overlaps_5pc_filt_peak=cageQTLTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$gene%>%subset(isFracFilt))
# cageQTLTable$overlaps_TSS_peak=cageQTLTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$main)
cageQTLTable$overlaps_5pc_filt_peak=cageQTLTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$main)





cageQTLTable%>%summarise(
	total = n(),
	Peaks=overlapsAnyPeakNoFilt%>%sum,
	RNA_filtered_Peaks=overlapsAnyPeak%>%sum,
	Fifty_plus_tags=overlaps_score_50_peak%>%sum,
	Within_gene=overlaps_gene_peak%>%sum,
	TSS_Peaks=overlaps_TSS_peak%>%sum,
	Five_pc_of_gene_tags=overlaps_5pc_filt_peak%>%sum
)%>%gather(filter,number)%>%
{qplot(data=.,x=filter,fill=I('blue'),y=number,geom='bar',stat='identity')+
scale_x_discrete(limits=.$filter%>%rev)+theme_bw()+ggtitle('percentage of QTLs effected by peak filters:location')+
coord_flip()}%T>%ggsave(plot=.,'/g/furlong/Harnett/TSS_CAGE_myfolder/feature_analysis/eqtl_filter_loss_loc.pdf')




#now by using the locations of the windows they effect
cageQTLgeneTable = 
cageQTLTable%>%
mutate(seqnames = str_extract(Window,'[^_]+'))%>%
mutate(start = str_match(Window,'_(\\d+)')%>%e(TRUE,2)%>%as.numeric)%>%
mutate(end = str_match(Window,'_(\\d+)_(\\d+)')%>%e(TRUE,3)%>%as.numeric)

setkey(cageQTLgeneTable,seqnames,start,end)
allsmoothmain%<>%GR2DT%>%setkey(seqnames,gene_name,start,end)
allsmoothinternal%<>%GR2DT%>%setkey(seqnames,gene_name,start,end)



ggdf=list(
	all = cageQTLgeneTable%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal),total=n()),
	strandswitch = cageQTLgeneTable%>%filter(StrandSwitch)%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal),total=n()),
	Redistribution = cageQTLgeneTable%>%filter(Redistribution)%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal),total=n()),
	RNAconc = cageQTLgeneTable%>%filter(validRNA & RNA)%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal),total=n()),
	RNANonConc = cageQTLgeneTable%>%filter(validRNA & (!RNA))%>%summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal),total=n()),
	Directional = cageQTLgeneTable%>%filter(Directional) %>% summarise(Main=sum(overlapsMain),internal=sum(overlapsInternal),total=n())
)%>%stackdfs('set')%>%gather(peaktype,number,Main,internal)

ggdf%<>%group_by(set)%>%mutate(frac = number/sum(total))

ggdf%>%{qplot(data=.,x=set,fill=peaktype,y=number,geom='bar',stat='identity',position='dodge')+coord_flip()}%T>%ggsave(plot=.,'/g/furlong/Harnett/TSS_CAGE_myfolder/feature_analysis/eqtl_peaktype_prop.pdf')

qplot(data=ggdf,x=set,fill=peaktype,y=frac,geom='bar',stat='identity',position='dodge')%T>%ggsave(plot=.,'/g/furlong/Harnett/TSS_CAGE_myfolder/feature_analysis/eqtl_peaktype_prop_frac.pdf')



cageQTLgeneTable$overlapsAnyPeakNoFilt=cageQTLgeneTable%>%DT2GR%>%overlapsAny(allsmoothpeaks[['tp68hembryo']]$nofilt%>%subset(score>0))
cageQTLgeneTable$overlapsAnyPeak=cageQTLgeneTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$all%>%subset(score>0))
cageQTLgeneTable$overlaps_score_50_peak=cageQTLgeneTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$all%>%subset(score>50))
cageQTLgeneTable$overlaps_TSS_peak=cageQTLgeneTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$gene%>%subsetByOverlaps(tssregions))
# cageQTLgeneTable$overlaps_5pc_filt_peak=cageQTLgeneTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$gene%>%subset(isFracFilt))
# cageQTLgeneTable$overlaps_TSS_peak=cageQTLgeneTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$main)
cageQTLgeneTable$overlaps_5pc_filt_peak=cageQTLgeneTable%>%DT2GR%>%overlapsAny( allsmoothpeaks[['tp68hembryo']]$main)

cageQTLgeneTable%>%summarise(
	total = n(),
	Peaks=overlapsAnyPeakNoFilt%>%sum,
	RNA_filtered_Peaks=overlapsAnyPeak%>%sum,
	Fifty_plus_tags=overlaps_score_50_peak%>%sum,
	TSS_Peaks=overlaps_TSS_peak%>%sum,
	Five_pc_of_gene_tags=overlaps_5pc_filt_peak%>%sum

)%>%gather(filter,number)%>%
{qplot(data=.,x=filter,fill=I('blue'),y=number,geom='bar',stat='identity')+
scale_x_discrete(limits=.$filter%>%rev)+theme_bw()+ggtitle('percentage of QTLs effected by peak filters:peak effected')+
coord_flip()}%T>%ggsave(plot=.,'/g/furlong/Harnett/TSS_CAGE_myfolder/feature_analysis/eqtl_filter_loss_target.pdf')



#####Nacho worried that we're excluding some high peaks 
peaks = allsmoothpeaks[['tp68hembryo']]$gene
peaks$intssregion = allsmoothpeaks[['tp68hembryo']]$gene%>%overlapsAny(tssregions)

library(gridExtra)

peaks[,w('intssregion score')]%>%unique%>%mergeByOverlaps(generegions)%>%mergeGR2DT%>%
	group_by(gene_id)%>%
	mutate(score = score/sum(score))%>%
	# {qplot(data=.,y=score,color=intssregion,x=intssregion)}
	# {qplot(data=.,x=score,color=intssregion,geom='density')}
	# {qplot(data=.,x=score,position='dodge',fill=intssregion,geom='histogram')}
	{
		grid.arrange( 
			filter(.,intssregion)%>%qplot(main='in tss regions',data=.,x=score,position='dodge',fill=intssregion,geom='histogram'),
			filter(.,intssregion)%>%qplot(main='in tss regions (yzoom)',data=.,x=score,position='dodge',fill=intssregion,geom='histogram',ylim=c(0,1e3)),
			filter(.,!intssregion)%>%qplot(main='not in tss regions',data=.,x=score,position='dodge',fill=intssregion,geom='histogram'),
			filter(.,!intssregion)%>%qplot(main='not in tss regions (yzoom)',data=.,x=score,position='dodge',fill=intssregion,geom='histogram',ylim=c(0,1e3)),
			nrow=4)
	}
	
	# {qplot(data=.,x=score,position='dodge',fill=intssregion,geom='histogram')}
	# {qplot(data=.,x=score,position='dodge',fill=intssregion,geom='histogram',log='y')}

grid.arrange( 
	x%>%data.frame(y=.)%>%{qplot(data=.,y=y,x=seq_along(y),geom='bar',stat='identity')},
	v%>%data.frame(y=.)%>%{qplot(data=.,y=y,x=seq_along(y),geom='bar',stat='identity')},
	# v3%>%data.frame(y=.)%>%{qplot(data=.,y=y,x=seq_along(y),geom='bar',stat='identity')},
	#v3%>%data.frame(y=.)%>%{qplot(data=.,y=y,x=seq_along(y),geom='bar',stat='identity')},
	(v==roll_max(v,n_smooth,fill=0))%>%data.frame(y=.)%>%{qplot(data=.,y=y,x=seq_along(y),geom='bar',stat='identity')},
	(v==roll_min(v,n_smooth,fill=0))%>%data.frame(y=.)%>%{qplot(data=.,y=y,x=seq_along(y),geom='bar',stat='identity')}
, nrow=3)
