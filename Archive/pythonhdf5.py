import h5py
import numpy as np
fileName='/g/furlong/project/24_TSSCAGE/analysis/FULLDATA_HDF5/dataWithPC1-5qqnorm_PowerLaw.hdf5'
# fileName ='/g/furlong/Harnett/TSS_CAGE_myfolder/data/hdf5/myDatahdf5.hdf5'
obhdf5=h5py.File(fileName)#open it
keys=obhdf5.keys()#get the names

#filter out the ones we don't want
keys=filter(lambda x: not x in ['genotype','panama','phenotype'], keys)#exclude geneotype


#delete the ones we don't want
for key in keys:
  del obhdf5[key]



