
setwd ( '/g/furlong/Harnett/TSS_CAGE_myfolder/' )
source ( 'src/tss_cage_functions.R' )
#load the annotation data with scores
load('data/objects/scored_regions.object.R')
load('data/objects/gene.annotation.object.R')
load('data/objects/accession.df.object.R')
#load('data/objects/cg.mapfilt.pl.object.R')

load('data/objects/accession.df.object.R')
library(reshape2)
gn=10
gsigs=log(10^(seq(1,3,length.out=gn)))
#vector of library sizes
reptable=accession.df
reptable=reptable[reptable$tissue=='embryo',]
libsizes=reptable$library.size
libsizes=seq(10000000,100000000,length.out=nrow(reptable))
samplegroups=c('timepoint','line','collection','prep','seq')
reptable=reptable[,c('accession','seq','prep','collection','line','timepoint')]
reptable[['nestedcols']]=NA
for(line in unique(reptable[['line']])){
  subtab=reptable[reptable[['line']]==line,]
  lcols=as.integer(as.factor(subtab[['collection']]))
  reptable[reptable[['line']]==line,][['nestedcols']]=lcols
}#variable with collection number nested within line



#vector of noise magnitudes for each different factor (seq, techrep, collection(biorep),line)
noisemags=c('seq'=0.01,'prep'=0.5,'collection'=4,'line'=2,'timepoint'=4)
#create matrix of counts
simcountmat=matrix(rep(gsigs,length(libsizes)),nrow=length(gsigs),ncol=length(libsizes))
colnames(simcountmat)=reptable$acc
#for(reptype in names(noisemags) ){

for(reptype in samplegroups ){
  SD=noisemags[[reptype]]
  repvect=reptable[,reptype]
  for(repn in unique(repvect)){
    simcountmat[,repvect==repn] = rnorm(n=gn,
                                        mean=simcountmat[,repvect==repn],
                                        sd=SD
    )
  }
}
colnames(simcountmat)=reptable$acc
sim.melt = melt(simcountmat)
head(sim.melt)
colnames(sim.melt) <- c('Gene','lib','value')
sim.melt$Gene = as.factor(sim.melt$Gene)
var= 'line'

for(var in c(samplegroups,'nestedcols')){
  sim.melt[[var]] = as.factor(reptable[[var]][ match(sim.melt$lib,reptable$acc) ])
}

#let's fit a series of progressively more complicated models....
library(nlme)
attach(sim.melt)

#fit on the smallest possible data subset
sim.melt
head(sim.melt)
dat = sim.melt[sim.melt$Gene==1 & sim.melt$timepoint=='68h',]
lmfit = lm(formula = ,data=dat)
lmmfit = lme(data=dat,fixed = value ~ 1 ,random = ~ 1 | line/collection)
lmmfit

lg.lmefit= lme(fixed = value ~ Gene*timepoint,
               random = ~ 1  | line/Gene ,
               data=sim.melt)


f
length(unique(dat$prep))
lmfit = lm(formula = value ~ as.factor(Gene) * timepoint,
           data=sim.melt)



lg.lmefit= lme(fixed = value ~ Gene*timepoint,
                random = ~ 1  | line/Gene ,
                data=sim.melt)

lgt.lmefit = lme(fixed = value ~ Gene*timepoint,
                random = ~ 1  | line/Gene/timepoint ,
                data=sim.melt)

lgtc.lmefit = lme(fixed = value ~ Gene*timepoint,
                 random = ~ 1  | timepoint/Gene/line/collection ,
                 data=sim.melt)
