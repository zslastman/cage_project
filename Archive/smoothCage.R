# smoothCage
# Smooths the Cage at enhancers so we can find the peak
#  Dermot Harnett
#  Loads cage load annotation
#
setwd ( '/g/furlong/Harnett/TSS_CAGE_myfolder/ ' )
#A run scripts, functions etc.
source ( '/g/furlong/Harnett/TSS_CAGE_myfolder/src/tss_cage_functions.R' )
#Load libraries
#library(libnames)
#load data
load('data/objects/allcage.object.R')
load('/g/furlong/Harnett/TSS_CAGE_myfolder/data/objects/crm.annotation.object.R')





getSmoothMax<-function(v,returnSmoothed=F,w=300,sparval=0.9){
  #this functon takes in a vector and smoothes it, then finds local maxima within a set 
  #windowsize and outputs the tallest one 300 is appropriate for a sparval of 0.9 (roughly)
  require(zoo)
  pad=floor(w/2)
  sx=smooth.spline(v,spar=sparval)$y#smooth
  xz <- as.zoo(sx)#zoo object
  #now get the local maxima
  rxz = rollapply(xz, w, function(z) which.max(z)==ceiling(w/2))
  ispeak = index(rxz)[coredata(rxz)]
  # plot(sx)
   #since rollapply gives a vector shorter than the original, get indices
  ispeak = ispeak[!ispeak %in% c(1,length(v))]
  ispeak = ispeak [ which.max( sx[ispeak] ) ]#choose the one with the best 
}

smoothfunk = function(x){smooth.spline(x,spar=0.9)$y}

for(ch in names(v)){
  for(i in 1:length(v[[ch]])){


  }
}

w=300
grname='/g/furlong/project/24_TSSCAGE/crm_testing//olga_eRNA_test_cad_crms.bed'

testbednames = c( '/g/furlong/project/24_TSSCAGE/crm_testing//olga_eRNA_test_crms.bed',
   '/g/furlong/project/24_TSSCAGE/crm_testing//olga_eRNA_test_cad_crms.bed',
    '/g/furlong/project/24_TSSCAGE/crm_testing//olga_neg_eRNA_test_crms.bed',
     '/g/furlong/project/24_TSSCAGE/crm_testing//olga_neg_eRNA_test_cad.bed')
for(grname in testbednames){
  for(strand in c('pos','neg')){
    gr=import(grname,asRangedData=F)
    seqlevels(gr)<-chrs.keep
    seqinfo(gr)<-si
    gr=sort(gr)
   # gr=gr[tmp]
    gr=resize(gr,width=width(gr)+(w*2),fix='center')
    v = GRViews(gr=gr,allcage$tp68hembryo[[strand]])
    maxs = viewApply(v,getSmoothMax)
    maxs=as(lapply(maxs,as.numeric),'SimpleIntegerList')
    maxs = unlist(maxs + start(v))
    foundpeak = !is.na(maxs)
    peakgr=GRanges(names(maxs)[foundpeak],IRanges(maxs[foundpeak],width=1),seqinfo=si,name=gr$name[foundpeak])
    peakgr$name = paste0('cs_',peakgr$name)
    export(peakgr,con= peakfilename )
    v2 = GRViews(gr=gr,coverage(peakgr,weight=100)+coverage(resize(gr,width=width(gr)-(w*2),fix='center'),weight=10))
    # pdf('analysis/smoothCage/smoothpeaksites.pdf')

    for(ch in names(v)){
      for(i in 1:length(v[[ch]])){
        pdf(paste0('analysis/smoothCage/',grname,'smoothpeaksites','_',ch,'_',i,'.pdf'))
      print(coverageplot(v[[ch]],v2[[ch]],i=i))
      dev.off()
      }
    }
}
}
 
#visualize the example
ch=as.character(seqnames( gr[tmp] ))
n=which(start(v[[ch]])==start(gr[tmp]))
coverageplot(v[[ch]],v2[[ch]],i=n)

dir.create('analysis/smoothCage')

# dev.off()

#use getSmoothMax to find local maximua for a bunch of vectors

#Then we want to illustrate this with coverage plots

