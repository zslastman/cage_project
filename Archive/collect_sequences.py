"""The following contains a set of methods designed to interact with samtools to collect sequences on the basis of coordinates."""

import sys, subprocess

def get_seq_faidx(genomeFile,chromosome,start=None,end=None):
    """Returns a tuple containing a header and a sequence"""
    cmdLine = 'samtools faidx ' + genomeFile + ' ' + chromosome
    if start:
        cmdLine = cmdLine + ':' + start
        if end: cmdLine = cmdLine + '-' + end
    proc = subprocess.Popen(cmdLine, stdout=subprocess.PIPE, shell=True)
    results = proc.communicate()[0].split('\n')
    return (results[0],''.join(results[1:]))


def read_bed_file(bedFile):
    resultsList = []
    f = open(bedFile)
    for line in f:
        if len(line) > 1: resultsList.append(line.rstrip('\n').split('\t'))
    return resultsList


def formatResultForPatster(results):
    return results[0].rstrip('>') + ' \\' + results[1] + '\\'

genomeFile = '/g/furlong/genome/D.melanogaster/Dm3/fasta/d_melanogaster_UCSC_chr_name.fa'
bedFile = '/g/furlong/Harnett/TSS_CAGE_myfolder/dnase.peaks.bed'
myResults = read_bed_file(bedFile)[0:10]

for i in myResults:
    chromosome,start,end = i[:3]
    results = get_seq_faidx(genomeFile,chromosome,start,end)
    print formatResultForPatster(results)
    

#from the Granges, get the sequences

#paste in the necessary slashes
#write all this as a table
