import os
import sys
os.chdir('/homes/harnett/statgenom-drosophilacage/pysrc/experiments/')
sys.path.append('./../')
sys.path.append('./../include')
assert(os.path.isdir('./../include'))
sys.path.append('/nfs/research/stegle/projects/limix')

from CFG.default import *


import scipy as SP
import scipy.linalg as LA
import limix
import limix.utils.preprocess as PRE
import limix.modules.varianceDecomposition as VAR
import data as DATA
import qtl as QTL
import h5py

#bash script for individual runs
# out=/g/furlong/Harnett/TSS_CAGE_myfolder/analysis/var_decomp/pl.mapfilt
# maxfold=800
# for j in `seq 0 $maxfold`
# do
# bsub -e panamafixed_perm_individualruns.error -o panamafixed_perm_individualruns.output python analysis.dermot.var.decomp.py  $out $maxfold $j perm
# done

# #take command line arguments
out_folder = sys.argv[1]
nfolds = int(sys.argv[2])
j = int(sys.argv[3])
data=sys.argv[5]

# # #or for trial run
#  out_folder = "/homes/harnett/vardecompout_mydatahdf.cg.mapfilt.pl"
#  nfolds = 800
#  j = 102 #say
# data = '/homes/harnett/mydatahdf.cg.mapfilt.pl.hdf5'
# #out file
assert os.path.isfile(data) 
# data = os.path.join(CFG['files']['data_dir'],'dataWithPC1-5qqnorm_PowerLaw.hdf5')
# out_path = os.path.join(CFG['files']['out'],out_folder)


#run if this is the main script
# if __name__ == '__main__':

#if the perm argument is there in the command line, read it in from the defaults
# perm = None
# if 'perm' in sys.argv:
#     perm = CFG['n_perm']
#     print "\nset n_perm = %d"%CFG['n_perm']
perm=500
#use a function to read in the data as a n object
data = DATA.QTLData(data)

#0. split in diff parts
genes = SP.array(data.geneIDs)#read gene.ids from the data
n_genes = genes.shape[0]
SP.random.seed(0)
Icv = SP.floor(nfolds*SP.arange(n_genes)/n_genes)
I = Icv==j
genes = list(genes[I])

gene=genes[1]
Y,dev_stage = data.getPhenotypes(gene)
assert Y is not None
N,P = Y.shape
stagevars = SP.zeros([len(genes),P,3])
vcworks = SP.ones(len(genes))

for gene in genes:
    print('.')
    i = genes.index(gene)
    # gene=genes[i]
    # print "%d  .. gene %s"% (len(genes)-i,gene)    
    Xcis, pos = data.getGenotypes(geneID=gene)#get genotypes for the gene
    genePos = data.getGenePos(gene)#actual position from the gene name

    #gbuild kinship matrix 
    Kpop    = data.Kpop
    Kcis    = SP.dot(Xcis,Xcis.T)
    Ktrans  = Kpop-Kcis
    Kpop   /= Kpop.diagonal().mean()
    Kcis   /= Kcis.diagonal().mean()
    Ktrans /= Ktrans.diagonal().mean()
    # 
    for p in range(P):
        # tmp=data.getPhenotypes(gene,dev_stage = dev_stage[p] ,center=False)[0]
        y = data.getPhenotypes(gene, dev_stage= dev_stage[p] )[0]
        if y == None : 
            stagevars[i,p,:] = 0
            continue
        # stagevars[i,p,:] = data.getPhenotypes(gene,dev_stage = dev_stage[p] ,center=False)[0].mean()
        # tmp =  data.getPhenotypes(gene,dev_stage = dev_stage[p] ,center=False)[0]
        vc = VAR.CVarianceDecomposition(y)
        vc.addSingleTraitTerm(Kcis)
        vc.addSingleTraitTerm(Ktrans)
        vc.addSingleTraitTerm(is_noise=True)
        fixed = SP.ones([N,1])
        vc.addFixedTerm(fixed)
        conv = vc.findLocalOptimum(init_method='random',verbose=False)
        stagevars[i,p,:]=vc.getVariances()

#create an 'unprocessed' hdf5 file
# genes=SP.array(genes)[vcworks]
# stagevars==stagevars[vcworks,:,:]
fout_name='tmp.%s.hdf5' % j

#create output file
fout_name = os.path.join(out_folder,fout_name)
if os.path.isfile(fout_name) : os.remove(fout_name)#delete if already there
fout = h5py.File(fout_name,'w')#open it and collect object handle

stagevar_group = fout.create_group('stagevars')#create a subgroup in the hdf5 object
#create a dataset in the subgroup from the SP array stagevars
stagevar_group.create_dataset(name='stagevars',data=stagevars,dtype='f')
#create another dataset, this time just a 1d vector of row names
stagevar_group.create_dataset(name='genes',data=genes)
#close the boject
fout.close()
check=h5py.File(fout_name,'r')#open it again to check it's worked
check.get('stagevars/genes')#view the subgroup names

# stagevar_group.create_dataset(name='stages',data=dev_stage)
# stagevar_group.create_dataset(name='components',data=('Cis','Trans','Noise'))





    #for each gene   
    Xcis, pos = data.getGenotypes(geneID=gene)#get genotypes for the gene
    genePos = data.getGenePos(gene)#actual position from the gene name

    #gbuild kinship matrix 
    Kpop    = data.Kpop
    Kcis    = SP.dot(Xcis,Xcis.T)
    Ktrans  = Kpop-Kcis
    Kpop   /= Kpop.diagonal().mean()
    Kcis   /= Kcis.diagonal().mean()
    Ktrans /= Ktrans.diagonal().mean()
    # 
    for p in range(P):#loop over stages
        y = data.getPhenotypes(gene, dev_stage= dev_stage[p] )[0]#get data for specific stage
        if y == None : 
            stagevars[i,p,:] = 0
            continue#skip if not data in hdf
        vc = VAR.CVarianceDecomposition(y)#vc object
        vc.addSingleTraitTerm(Kcis)#add cis matrix
        vc.addSingleTraitTerm(Ktrans)#trans
        vc.addSingleTraitTerm(is_noise=True)#noise
        fixed = SP.ones([N,1])#fixed
        vc.addFixedTerm(fixed)
        conv = vc.findLocalOptimum(init_method='random',verbose=False)#carry out decomposition
        stagevars[i,p,:]=vc.getVariances()#collect results in array




