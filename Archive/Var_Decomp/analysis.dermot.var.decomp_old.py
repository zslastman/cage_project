import os
import sys
os.chdir('/homes/harnett/statgenom-drosophilacage/pysrc/experiments/')
sys.path.append('./../')
sys.path.append('./../include')
assert(os.path.isdir('./../include'))
sys.path.append('/nfs/research/stegle/projects/limix')

from CFG.default import *


import scipy as SP
import scipy.linalg as LA

import limix
import limix.utils.preprocess as PRE
import limix.modules.varianceDecomposition as VAR

# from utils import export

import data as DATA
import qtl as QTL

# import cPickle
# import pdb
# import time
import h5py

# CFG['files'] = {}

# # limix path
# CFG['limix_path'] = '/nfs/research/stegle/projects/limix'



# ###FIle locations and directories ####
# # file locations
# CFG['files']['base_dir']   = "~/tmp"
# CFG['files']['data_dir']   = os.path.join(CFG['files']['base_dir'],'data')
# CFG['files']['pheno']      = os.path.join(CFG['files']['data_dir'],'phenotypes')
# CFG['files']['geno']       = os.path.join(CFG['files']['data_dir'],'genotypes')
# #CFG['files']['geno_file'] = os.path.join(CFG['files']['geno'],'freeze2.bins.vcf')
# CFG['files']['geno_file']  = os.path.join(CFG['files']['geno'],'all.filtered.nochr.vcf')
# CFG['files']['geno_test']  = os.path.join(CFG['files']['geno'],'test.vcf')
# CFG['files']['out']        = os.path.join(CFG['files']['base_dir'],'out')

# # Filter for the preprocessing part
# CFG['qtl'] = {}
# CFG['qtl']['filter_maf']     = 0.05
# CFG['qtl']['filter_missing'] = 0.1
# CFG['qtl']['cis_window']     = 1E5

# # FDR threshold
# CFG['fdr_threshold'] = 0.01

# # permutations
# CFG['n_perm'] = 500




#bash script for individual runs
# out=/g/furlong/Harnett/TSS_CAGE_myfolder/analysis/var_decomp/pl.mapfilt
# maxfold=800
# for j in `seq 0 $maxfold`
# do
# bsub -e panamafixed_perm_individualruns.error -o panamafixed_perm_individualruns.output python analysis.dermot.var.decomp.py  $out $maxfold $j perm
# done

# #take command line arguments
out_folder = sys.argv[1]
nfolds = int(sys.argv[2])
j = int(sys.argv[3])

# # #or for trial run
#  out_folder = "~/tmp"
#  nfolds = 800
#  j = 102 #say

# #out file
# data = os.path.join(CFG['files']['data_dir'],'dataWithPC1-5qqnorm_PowerLaw.hdf5')
# out_path = os.path.join(CFG['files']['out'],out_folder)

# data = '/homes/harnett/myDatahdf5.hdf5'
data=sys.argv[5]
assert os.path.isfile(data) 

#run if this is the main script
# if __name__ == '__main__':

#if the perm argument is there in the command line, read it in from the defaults
# perm = None
# if 'perm' in sys.argv:
#     perm = CFG['n_perm']
#     print "\nset n_perm = %d"%CFG['n_perm']
perm=500
#use a function to read in the data as a n object
data = DATA.QTLData(data)

#0. split in diff parts
genes = SP.array(data.geneIDs)#read gene.ids from the data
n_genes = genes.shape[0]
SP.random.seed(0)
Icv = SP.floor(nfolds*SP.arange(n_genes)/n_genes)
I = Icv==j
genes = list(genes[I])

#create an 'unprocessed' hdf5 file
fout_name='tmp.%s.hdf5' % j
fout_name = os.path.join(out_folder,fout_name)
if os.path.isfile(fout_name) : os.remove(fout_name)
fout = h5py.File(fout_name,'w')





Y,dev_stage = data.getPhenotypes(genes[1])
N,P = Y.shape
genes=genes
stagevars = SP.zeros([len(genes),P,3])
nodata = 

for gene in genes:
    
    i = genes.index(gene)
    # gene=genes[i]

    print ""
    # print "%d  .. gene %s"% (len(genes)-i,gene)
    
    Y, dev_stage = data.getPhenotypes(gene)#get pheno matrix and developmental stage
    if Y==None :  continue#skip if no pheno information for this gene
    Xcis, pos = data.getGenotypes(geneID=gene)#get genotypes for the gene
    genePos = data.getGenePos(gene)#actual position from the gene name
    N, P = Y.shape #dimensions of pheno matrix

    #gbuild kinship matrix 
    Kpop    = data.Kpop
    Kcis    = SP.dot(Xcis,Xcis.T)
    Ktrans  = Kpop-Kcis
    Kpop   /= Kpop.diagonal().mean()
    Kcis   /= Kcis.diagonal().mean()
    Ktrans /= Ktrans.diagonal().mean()

    for p in range(P):
        y = Y[:,p:p+1]
        vc = VAR.CVarianceDecomposition(y)
        vc.addSingleTraitTerm(Kcis)
        vc.addSingleTraitTerm(Ktrans)
        vc.addSingleTraitTerm(is_noise=True)
        fixed = SP.ones([N,1])
        vc.addFixedTerm(fixed)
        conv = vc.findLocalOptimum(init_method='random',verbose=False)
        # stagevars[i,p,:]=
        #stagevars[i,p,:]=vc.getVariances()




stagevar_group = fout.create_group('stagevars')
stagevar_group.create_dataset(name='stagevars',data=stagevars,dtype='f')
stagevar_group.create_dataset(name='genes',data=genes)

fout.close()
check=h5py.File(fout_name,'r')
# stagevar_group.create_dataset(name='stages',data=dev_stage)
# stagevar_group.create_dataset(name='components',data=('Cis','Trans','Noise'))







            # y = copy.deepcopy(Y[:,p:p+1])
            # y *= 100
            # vc = VAR.CVarianceDecomposition(y)
            # vc.addSingleTraitTerm(Kcis)
            # vc.addSingleTraitTerm(Ktrans)
            # vc.addSingleTraitTerm(is_noise=True)
            # fixed = SP.ones([N,1])
            # vc.addFixedTerm(fixed)
            # conv = vc.findLocalOptimum(init_method='random',verbose=False)
            # stagevars[i,p,:]=vc.getVariances()
            # tmpscaled=stagevars[i,p,:]



# #        vc.addMultiTraitTerm(Ktrans)
# #        vc.addMultiTraitTerm(is_noise=True)
# #        fixed = SP.ones([N,1])
# #        vc.addFixedTerm(fixed)
# #        conv = vc.findLocalOptimum(init_method='empCov',verbose=False)
         #   export(trait_group,'cis_trans_noise_vd',vc.getVariances())


        #1. get pheno and genp data from gene g
        #get 

        # Yqq, mn      = data.getPhenotypes(gene,qq=True)
        # Ypowerlaw, mn      = data.getPhenotypes(gene,append='_PowLaw')
        #Ypowerlawmap, mn      = data.getPhenotypes(gene,append='_PowLawMap')
        # Ypca1, mn    = data.getPC(gene,pc=1)
        # Ypca1qq, mn  = data.getPC(gene,pc=1,qq=True)
        # Ypca2, mn    = data.getPC(gene,pc=2)
        # Ypca2qq, mn  = data.getPC(gene,pc=2,qq=True)



#         # if Y==None or Yqq==None or Ypca1==None or Ypca2==None or Ypca1qq==None or Ypca2qq==None or Ypowerlaw == None or Ypowerlawmap == None:     continue
#         if Y==None or Ypowerlawmap == None:     continue
        
         
        
#         
        
#         

        
#         #2. build kinship
#         Kpop    = data.Kpop
#         Kcis    = SP.dot(Xcis,Xcis.T)
#         Ktrans  = Kpop-Kcis
#         Kpop   /= Kpop.diagonal().mean()
#         Kcis   /= Kcis.diagonal().mean()
#         Ktrans /= Ktrans.diagonal().mean()
#         Kpanama = {}
#         Kpanama['Kpanama_all'] = data.getKpanama()
#         Kpanama['Kpanama_0']   = data.getKpanama(0)
#         Kpanama['Kpanama_1']   = data.getKpanama(1)
#         Kpanama['Kpanama_2']   = data.getKpanama(2)
        
#         #3. export some basic info
#         gene_group = fout.create_group(gene)
#         gene_group.create_dataset('dev_stage', data=dev_stage)
#         gene_group.create_dataset('pos',data=pos)
#         gene_group.create_dataset('genePos',data=genePos)
#         gene_group.create_dataset('Xcis',data=Xcis)
#         gene_group.create_dataset('Y',data=Y)
#         gene_group.create_dataset('Yqq',data=Yqq)
#         gene_group.create_dataset('Ypca1',data=Ypca1)
#         gene_group.create_dataset('Ypca2',data=Ypca2)
#         gene_group.create_dataset('Ypca1qq',data=Ypca1qq)
#         gene_group.create_dataset('Ypca2qq',data=Ypca2qq)
#         gene_group.create_dataset('Ypowerlaw',data=Ypowerlaw)
#         gene_group.create_dataset('Ypowerlawmap',data=Ypowerlawmap)
 
#         print ".. single trait analysis"
#         #4. single trait analysis
#         for p in range(P):
            
#             trait_group = gene_group.create_group('trait%d'%p)
            
#             print "   .. analysis trait %d"%p
#             y = Y[:,p:p+1]
            
#             print "      .. variance decomposition cis/trans/noise"
#             vc = VAR.CVarianceDecomposition(y)
#             vc.addSingleTraitTerm(Kcis)
#             vc.addSingleTraitTerm(Ktrans)
#             vc.addSingleTraitTerm(is_noise=True)
#             fixed = SP.ones([N,1])
#             vc.addFixedTerm(fixed)
#             conv = vc.findLocalOptimum(init_method='random',verbose=False)
#             export(trait_group,'cis_trans_noise_vd',vc.getVariances())
            
#             print "      .. variance decomposition geno/noise"
#             vc = VAR.CVarianceDecomposition(y)
#             vc.addSingleTraitTerm(Kpop)
#             vc.addSingleTraitTerm(is_noise=True)
#             fixed = SP.ones([N,1])
#             vc.addFixedTerm(fixed)
#             conv = vc.findLocalOptimum(init_method='random',verbose=False)
#             export(trait_group,'geno_noise_vd',vc.getVariances())

#             y = Ypowerlaw[:,p:p+1]            
#             print "      .. lmm scan power law"
#             rv = QTL.simple_lmm(Xcis,y,K=Ktrans,covs=fixed,n_perm=perm)
#             export(trait_group,'lmm_scan_powerlaw',rv)

#             print "      .. lmm scan with panama"
#             rv = QTL.simple_lmm(Xcis,y,K=Kpanama['Kpanama_%d'%p],covs=fixed,n_perm=perm)
#             export(trait_group,'lmm_scan_powerlaw_panama',rv)


#             y = Ypowerlawmap[:,p:p+1]
#             print "      .. lmm scan power law"
#             rv = QTL.simple_lmm(Xcis,y,K=Ktrans,covs=fixed,n_perm=perm)
#             export(trait_group,'lmm_scan_powerlawmap',rv)

#             print "      .. lmm scan with panama"
#             rv = QTL.simple_lmm(Xcis,y,K=Kpanama['Kpanama_%d'%p],covs=fixed,n_perm=perm)
#             export(trait_group,'lmm_scan_powerlawmap_panama',rv)

# #            print "      .. lmm scan"
# #            rv = QTL.simple_lmm(Xcis,y,K=Ktrans,covs=fixed,n_perm=perm)
# #            export(trait_group,'lmm_scan',rv)
# #            
# #            #print "      .. lmm scan with panama global"
# #            #rv = QTL.simple_lmm(Xcis,y,K=Kpanama['Kpanama_all'],covs=fixed,n_perm=perm)
# #            #export(trait_group,'lmm_scan_panglob',rv)
# #            
# #            print "      .. lmm scan with panama"
# #            rv = QTL.simple_lmm(Xcis,y,K=Kpanama['Kpanama_%d'%p],covs=fixed,n_perm=perm)
# #            export(trait_group,'lmm_scan_panama',rv)
# #            
# #            print "      .. lmm scan first PC"
# #            y = Ypca1[:,p:p+1]
# #            rv = QTL.simple_lmm(Xcis,y,K=Ktrans,covs=fixed,n_perm=perm)
# #            export(trait_group,'lmm_scan_pc1',rv)
# #
# #            print "      .. lmm scan second PC"
# #            y = Ypca2[:,p:p+1]
# #            rv = QTL.simple_lmm(Xcis,y,K=Ktrans,covs=fixed,n_perm=perm)
# #            export(trait_group,'lmm_scan_pc2',rv)
# #            
# #            #print "      .. lmm scan qq"
# #            #y = Yqq[:,p:p+1]
# #            #rv = QTL.simple_lmm(Xcis,y,K=Ktrans,covs=fixed,n_perm=perm)
# #            #export(trait_group,'lmm_scan_qq',rv)
# #            
# #            #print "      .. lmm scan first PC qq"
# #            #y = Ypca1qq[:,p:p+1]
# #            #rv = QTL.simple_lmm(Xcis,y,K=Ktrans,covs=fixed,n_perm=perm)
# #            #export(trait_group,'lmm_scan_pc1qq',rv)
# #            
# #            #print "      .. lmm scan second PC qq"
# #            #y = Ypca2qq[:,p:p+1]
# #            #rv = QTL.simple_lmm(Xcis,y,K=Ktrans,covs=fixed,n_perm=perm)
# #            #export(trait_group,'lmm_scan_pc2qq',rv)
# #
# #        
# #        print ".. joint analysis"
# #
# #        joint_group = gene_group.create_group('joint')
# #
# #        #4. multi-trait variance deocomposition
# #        print "   .. joint cis, trans, noise variance decomposition"
# #        vc = VAR.CVarianceDecomposition(Y)
# #        vc.addMultiTraitTerm(Kcis)
# #        vc.addMultiTraitTerm(Ktrans)
# #        vc.addMultiTraitTerm(is_noise=True)
# #        fixed = SP.ones([N,1])
# #        vc.addFixedTerm(fixed)
# #        conv = vc.findLocalOptimum(init_method='empCov',verbose=False)
# #
# #        vd_group = joint_group.create_group('cis_trans_noise_vd')
# #        export(vd_group,'empTraitCov',vc.getEmpTraitCovar())
# #        export(vd_group,'cisTraitCov',vc.getEstTraitCovar(0))
# #        export(vd_group,'transTraitCov',vc.getEstTraitCovar(1))
# #        export(vd_group,'noiseTraitCov',vc.getEstTraitCovar(2))
# #        export(vd_group,'context',SP.array([SP.var(vc.getFixed())]))
# #
# #
# #        #5. geno-noise decomposition
# #        print "   .. geno, noise variance decomposition"
# #        vc = VAR.CVarianceDecomposition(Y)
# #        vc.addMultiTraitTerm(Kpop)
# #        vc.addMultiTraitTerm(is_noise=True)
# #        fixed = SP.ones([N,1])
# #        vc.addFixedTerm(fixed)
# #        conv = vc.findLocalOptimum(init_method='random',fast=True,verbose=False)
# #        
# #        traitGeno  = vc.getEstTraitCovar(0)
# #        traitNoise = vc.getEstTraitCovar(1)
# #
# #        vd_group = joint_group.create_group('geno_noise_vd')
# #        export(vd_group,'empTraitCov',vc.getEmpTraitCovar())
# #        export(vd_group,'geonTraitCov',vc.getEstTraitCovar(0))
# #        export(vd_group,'noiseTraitCov',vc.getEstTraitCovar(1))
# #        
# #        Kbg = SP.kron(traitGeno,Kpop)+SP.kron(traitNoise,SP.eye(N))
# #
# #        #6. common effect test
# #        print "   .. common effect test"
# #        rv = QTL.common_effect_test(Xcis,Y,Kbg)
# #        export(joint_group,'comm_eff_test',rv)
# #
# #
# #        #7. interaction effect test
# #        print "   .. interaction effect test"
# #        rv = QTL.GxE_interaction_test(Xcis,Y,Kbg)
# #        export(joint_group,'int_eff_test',rv)
# #
# # y = Ypca1
# #
# #        print ".. joint analysis pc1"
# #
# #        joint_group = gene_group.create_group('joint_pc1')
# #
# #        #4b. multi-trait variance deocomposition
# #        print "   .. joint cis, trans, noise variance decomposition"
# #        vc = VAR.CVarianceDecomposition(y)
# #        vc.addMultiTraitTerm(Kcis)
# #        vc.addMultiTraitTerm(Ktrans)
# #        vc.addMultiTraitTerm(is_noise=True)
# #        fixed = SP.ones([N,1])
# #        vc.addFixedTerm(fixed)
# #        conv = vc.findLocalOptimum(init_method='empCov',verbose=False)
# #
# #        vd_group = joint_group.create_group('cis_trans_noise_vd')
# #        export(vd_group,'empTraitCov',vc.getEmpTraitCovar())
# #        export(vd_group,'cisTraitCov',vc.getEstTraitCovar(0))
# #        export(vd_group,'transTraitCov',vc.getEstTraitCovar(1))
# #        export(vd_group,'noiseTraitCov',vc.getEstTraitCovar(2))
# #        export(vd_group,'context',SP.array([SP.var(vc.getFixed())]))
# #
# #
# #        #5b. geno-noise decomposition
# #        print "   .. geno, noise variance decomposition"
# #        vc = VAR.CVarianceDecomposition(y)
# #        vc.addMultiTraitTerm(Kpop)
# #        vc.addMultiTraitTerm(is_noise=True)
# #        fixed = SP.ones([N,1])
# #        vc.addFixedTerm(fixed)
# #        conv = vc.findLocalOptimum(init_method='random',fast=True,verbose=False)
# #
# #        traitGeno  = vc.getEstTraitCovar(0)
# #        traitNoise = vc.getEstTraitCovar(1)
# #
# #        vd_group = joint_group.create_group('geno_noise_vd')
# #        export(vd_group,'empTraitCov',vc.getEmpTraitCovar())
# #        export(vd_group,'geonTraitCov',vc.getEstTraitCovar(0))
# #        export(vd_group,'noiseTraitCov',vc.getEstTraitCovar(1))
# #
# #        Kbg = SP.kron(traitGeno,Kpop)+SP.kron(traitNoise,SP.eye(N))
# #
# #        #6b. common effect test
# #        print "   .. common effect test"
# #        rv = QTL.common_effect_test(Xcis,y,Kbg)
# #        export(joint_group,'comm_eff_test',rv)
# #
# # #7b. interaction effect test
# #        print "   .. interaction effect test"
# #        rv = QTL.GxE_interaction_test(Xcis,y,Kbg)
# #        export(joint_group,'int_eff_test',rv)
# #

#         ##8. interaction effect test
#         #print "   .. forward lmm test"
#         #rv = QTL.forward_lmm(Xcis,Y,Kbg)
#         #export(joint_group,'forward_lmm',rv)


#     fout.close()


