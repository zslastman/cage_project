#dispense with unneccessary hd5 objects, writes output to txt
import os
import sys
os.chdir('/homes/harnett/statgenom-drosophilacage/pysrc/experiments/')
sys.path.append('./../')
sys.path.append('./../include')
assert(os.path.isdir('./../include'))
sys.path.append('/nfs/research/stegle/projects/limix')

from CFG.default import *


import scipy as SP
import scipy.linalg as LA
import limix
import limix.utils.preprocess as PRE
import limix.modules.varianceDecomposition as VAR
import data as DATA
import qtl as QTL
import h5py

#bash script for individual runs
# out=/g/furlong/Harnett/TSS_CAGE_myfolder/analysis/var_decomp/pl.mapfilt
# maxfold=800
# for j in `seq 0 $maxfold`
# do
# bsub -e panamafixed_perm_individualruns.error -o panamafixed_perm_individualruns.output python analysis.dermot.var.decomp.py  $out $maxfold $j perm
# done

# #take command line arguments
out_folder = sys.argv[1]
nfolds = int(sys.argv[2])
j = int(sys.argv[3])
data=sys.argv[5]

# # #or for trial run
  # out_folder = "/homes/harnett/vardecompout_mydatahdf.cg.mapfilt.pl"
#  nfolds = 800
#  j = 102 #say
 # data = '/homes/harnett/mydatahdf.cg.mapfilt.pl.hdf5'
# #out file
assert os.path.isfile(data) 
# data = os.path.join(CFG['files']['data_dir'],'dataWithPC1-5qqnorm_PowerLaw.hdf5')
# out_path = os.path.join(CFG['files']['out'],out_folder)


#run if this is the main script
# if __name__ == '__main__':

#if the perm argument is there in the command line, read it in from the defaults
# perm = None
# if 'perm' in sys.argv:
#     perm = CFG['n_perm']
#     print "\nset n_perm = %d"%CFG['n_perm']
perm=500
#use a function to read in the data as a n object
data = DATA.QTLData(data)

#0. split in diff parts
genes = SP.array(data.geneIDs)#read gene.ids from the data
gene=genes[1]
n_genes = genes.shape[0]
SP.random.seed(0)
Icv = SP.floor(nfolds*SP.arange(n_genes)/n_genes)
I = Icv==j
genes = list(genes[I])

Y,dev_stage = data.getPhenotypes(gene)
assert Y is not None
N,P = Y.shape
stagevars = SP.zeros([len(genes),P,3])
vcworks = SP.ones(len(genes))

for gene in genes:
    # print('.')
    # i = genes.index(gene)
    # gene=genes[i]
    # print "%d  .. gene %s"% (len(genes)-i,gene)    
    Xcis, pos = data.getGenotypes(geneID=gene)#get genotypes for the gene
    genePos = data.getGenePos(gene)#actual position from the gene name
    # gbuild kinship matrix 
    Kpop    = data.Kpop
    Kcis    = SP.dot(Xcis,Xcis.T)
    Ktrans  = Kpop-Kcis
    Kpop   /= Kpop.diagonal().mean()
    Kcis   /= Kcis.diagonal().mean()
    Ktrans /= Ktrans.diagonal().mean()
    
    for p in range(P):
     
        y=data.getPhenotypes(gene,dev_stage = dev_stage[p] ,center=True)[0]

        if y == None : 
            y=SP.zeros(Y.shape[0])
            sys.stdout.write( 'VCoutput\t'+gene+'\t'+dev_stage[p]+'\t'+str(y.sum())+'\t'+str(y.sum())+'\t'+str(y.sum())+'\n' )
        else:
            vc = VAR.CVarianceDecomposition(y)
            vc.addSingleTraitTerm(Kcis)
            vc.addSingleTraitTerm(Ktrans)
            vc.addSingleTraitTerm(is_noise=True)
            fixed = SP.ones([N,1])
            vc.addFixedEffect(fixed)
            conv = vc.findLocalOptimum(init_method='random',verbose=False)
            var=vc.getVariances()
            # assert 0.99 < var.sum() < 1.01
            # fout.write( gene+'\t'+dev_stage[p]+'\t'+str(var[0])+'\t'+str(var[1])+'\t'+str(var[2])+'\n' )
            sys.stdout.write('VCoutput\t'+gene+'\t'+dev_stage[p]+'\t'+str(var[0])+'\t'+str(var[1])+'\t'+str(var[2])+'\n'  )


   # y=data.getPhenotypes(gene,dev_stage = dev_stage[p] ,center=True)[0]
        # if y == None : 
        #     y=SP.zeros(Y.shape[0])
        #     sys.stdout.write( 'VCoutput\t'+gene+'\t'+dev_stage[p]+'\t'+str(y.sum())+'\t'+str(y.sum())+'\t'+str(y.sum())+'\n' )
        # else:
        #     sys.stdout.write( 'VCoutput\t'+gene+'\t'+dev_stage[p]+'\t'+str(y.sum())+'\t'+str(y.sum())+'\t'+str(y.sum())+'\n' )



# stagevar_group.create_dataset(name='stages',data=dev_stage)
# stagevar_group.create_dataset(name='components',data=('Cis','Trans','Noise'))




# fout_name='tmp.%s.txt' % j
# #create output file
# fout_name = os.path.join(out_folder,fout_name)
# if os.path.isfile(fout_name) : os.remove(fout_name)#delete if already there
# fout = open(fout_name,'w')#open it and collect object handle

# genes = ['g1','g2']

# stgs=['s1','s2']

# P=[1,2]


#matches for example gene


# for gene in genes:
#     for p in range(2):
#             fout.write( gene+'\t'+stgs[p]+'\t'+str(P[0])+'\t'+str(P[1])+'\n' )

# fout.close()
# fout=open(fout_name,'r')
# fout.read()

# P.