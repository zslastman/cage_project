if [ $# -ne 2 ]
then
 echo "enter two arguments - the hdf5 file and test/nottest"
 exit
fi

#cd /homes/harnett/statgenom-drosophilacage/pysrc/experiments/

datahdf=$1
maxfold=800
out=/homes/harnett/vardecompout
bname=`basename $1`
bname=${bname%.*}
out="${out}_${bname}"


echo "creating analysis outfolder $out"

mkdir $out

echo 'submitting bjobs\n'


#loop submitting jobs doing decomp for various genes
for j in `seq 0 $maxfold`
do
  if [ $2 -ne 0 ]
  then
   echo "python analysis.dermot.var.decomp.py  $out $maxfold $j perm $datahdf"
   python analysis.dermot.var.decomp.py  $out $maxfold $j perm $datahdf
   echo "exiting loop early"
   break
  fi

  echo "python analysis.dermot.var.decomp.py  $out $maxfold $j perm $datahdf"
  bsub -e panamafixed_perm_individualruns.error -o panamafixed_perm_individualruns.output python analysis.dermot.var.decomp.py  $out $maxfold $j perm $datahdf
#break

done


echo 'waiting for bjobs to finish....'

a=`bjobs`
while [ ${#a} -gt 0 ]; do
    sleep 10
    a=`bjobs`
done

echo 'all bjobs are now done!'
cd /homes/harnett
echo "python summarize.dermot.py $out $datahdf"
python summarize.dermot.py $out $datahdf

echo "summary hdf5 created.\n"


