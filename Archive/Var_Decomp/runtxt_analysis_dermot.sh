if [ $# -ne 2 ]
then
 echo "enter two arguments - the hdf5 file and test/nottest"
 exit
fi

#cd /homes/harnett/statgenom-drosophilacage/pysrc/experiments/

datahdf=$1
maxfold=800
out=/homes/harnett/vardecompout
bname=`basename $1`
bname=${bname%.*}
out="${out}_${bname}/"


echo "creating analysis outfolder $out"

mkdir $out
rm "${out}/*"

echo 'submitting bjobs\n'


#loop submitting jobs doing decomp for various genes
for j in `seq 0 $maxfold`
do
  if [ $2 -ne 0 ]
  then
   echo "python vcanalysis.py  $out $maxfold $j perm $datahdf"
   python vcanalysis.py  $out $maxfold $j perm $datahdf > "${out}/tmp.${j}.txt"
   echo "exiting loop early"
   break
  fi

  echo "python analysis.dermot.var.decomp.py  $out $maxfold $j perm $datahdf"
   bsub -e panamafixed_perm_individualruns.error -o "${out}/tmp.${j}.txt" python vcanalysis.py  $out $maxfold $j perm $datahdf #> "${out}/tmp.${j}.txt" 
  # python vcanalysis.py  $out $maxfold $j perm $datahdf > "${out}/tmp.${j}.txt" 

  if j -eq 3 break
  then
   break
  fi


done

echo 'waiting for bjobs to finish....'

a=`bjobs`
while [ ${#a} -gt 0 ]; do
    sleep 10
    a=`bjobs`
done

echo 'all bjobs are now done!'

cd /homes/harnett

echo " cat ${out}/*.txt > ${out}/allvc.txt "

cat "${out}/tmp*.txt" | grep "^chr" > "${out}/allvc.txt"



# echo "python summarizetxt.dermot.py $out $datahdf"

# python summarizetxt.dermot.py $out $datahdf

# echo "summary hdf5 created.\n"


