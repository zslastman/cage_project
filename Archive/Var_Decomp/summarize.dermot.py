import sys
import h5py
import glob
import os
import scipy as SP
import numpy as np

folder = '/homes/harnett/vardecompout_mydatahdf.cg.mapfilt.pl/'
hdfname = 'homes/harnett/mydatahdf.cg.mapfilt.pl'
folder = sys.argv[1]
print(folder)
if 'ipython' in sys.argv[0] : folder= '/homes/harnett/tmp' 

datadict= {}            

fnames = os.path.join(folder,'tmp*.hdf5')
files = glob.glob(fnames)
files = SP.sort(files)

print "collating %d files" % len(files)

geneIDs = []
stagevararrays=[]
file=files[0]

for file in files:                
        f = h5py.File(file,'r')
        if len(f.keys())==0 : continue
        geneIDs.extend(list(f.get('stagevars/genes')[:]))
        stagevararrays.append( f.get('stagevars/stagevars')[:,:,:])
        f.close()

stagevararray = np.vstack(stagevararrays)
print(stagevararray.shape)
print(stagevararray[0,:,:])

assert len(sys.argv)==3
assert len(geneIDs) == stagevararray.shape[0]

hdfname=os.path.basename(sys.argv[2])
fout_name = os.path.join(folder,hdfname+'.summary.d.hdf5')
if os.path.isfile(fout_name) : os.remove(fout_name)
fout = h5py.File(fout_name,'w')#fout now a h5handle

sgroup = fout.create_group('stagevars')
sgroup.create_dataset(name='stagevararray',data=stagevararray)
sgroup.create_dataset(name='geneIDs',data=geneIDs)



fout.close()

check=h5py.File(name='vardecompout_mydatahdf.cg.mapfilt.pl/summary.d.hdf5')

