require(speedglm)



if(! 'testedSnpPairs' %in% ls() ) testedSnpPairs=testedSnpPairs = fread('../data/tested.Pars.txt')
testedSnpPairs%>%colnames


#first join the motif change data to 




################################################################################
##########Intersect snp-gene pairs with the change data
################################################################################
#load data on moitf changes
#add group variable to the change df, remove redundancies, and spread it like our 
#rad groups add cisbp stuff
# motifgroups%<>%rbind(data.frame(pwm = allvarMotifChange$pwm%>%unique%>%setminus(motifgroups$pwm) , group=4))
# motifgroups%>%writeTable('/g/furlong/Harnett/Tagseq_myfolder/analysis/threePrimeMotifs_withnew.txt')


#now remove reudunancies and make a logical column for each group
allvarMotifChangeGroup = 
  cgPromMotifChangeSimple%>%
  distinct(iscreated,name,group)%>%
  select(-pwm)%>%
  #merge created and motif cols
  mutate(motif = paste0('Group',group,'_',ifelse(iscreated,'created','destroyed')))%>%
  distinct(name,motif,strand)%>%
  select(name,motif,strand)

cgPromMotifChangeSimple$motif = paste0(cgPromMotifChangeSimple$pwm,'_',ifelse(cgPromMotifChangeSimple$iscreated,'created','destroyed'))

#merge created and motif cols
cgPromMotifChangeSimple%<>%mutate(motif = paste0(pwm,'_',ifelse(iscreated,'created','destroyed')))

#unique motifs


#column for each motif
cgPromMotifChangeSimple=
  cgPromMotifChangeSimple%>%mutate(tmp=TRUE)%>%distinct%>%spread(motif,tmp)%>%mutate_each(funs(.%>%replace(is.na(.),FALSE)))
allvarMotifChangeGroup=
  allvarMotifChangeGroup%>%mutate(tmp=TRUE)%>%spread(motif,tmp)%>%mutate_each(funs(.%>%replace(is.na(.),FALSE)))

allvarMotifChangeGroup%>%group_by(name,strand)%>%filter(n()>1)
cgPromMotifChangeSimple%>%group_by(name,strand)%>%filter(n()>1)


#put names back in to link to the changes
testedSnpPairs%<>%mutate(name=paste0(seqnames,'_',start)%>%str_replace('chr',''))
#now subset the snp gene pairs and link them to the changes
geneSnpMapMot = 
	testedSnpPairs%>%
  DT2GR%>%subsetByOverlaps(tssgr)%>%GR2DT%>%
	# DT2GR%>%subsetByOverlaps(cagepeakstssgr)%>%GR2DT%>%
	select(name,strand,one_of(testedSnpPairsQtls,unlist(modelvarlist)))%>%
	left_join(allvarMotifChangeGroup,by=w('name strand'))
  # %>%
  # left_join(cgPromMotifChangeSimple,by=w('name strand'))

geneSnpMapMot%<>%mutate_each(funs(.%>%replace(is.na(.),FALSE)))

#also add a new, 'both' column
# cdcols = c(colnames(cgPromMotifChangeSimple),colnames(allvarMotifChangeGroup))%>%str_extract(perl('(\\w+)_destroy'))%>%str_replace('_destroy','')%>%vfilt(.%>%is.na%>%not)
cdcols = c(colnames(allvarMotifChangeGroup))%>%str_extract(perl('(\\w+)_destroy'))%>%str_replace('_destroy','')%>%vfilt(.%>%is.na%>%not)
for(cdcol in cdcols){
   geneSnpMapMot[[paste0(cdcol,'_changed')]] =  geneSnpMapMot[[paste0(cdcol,'_destroyed')]]|  geneSnpMapMot[[paste0(cdcol,'_created')]]
}


require(speedglm)



################################################################################
##########Define Columns for regression
################################################################################

#list of models to run
modelvarlist = list(
  expr.minfreq =  w('neargeneexpr minfreq'),
  expr.minfreq.tagend =   w('neargeneexpr minfreq distance')
)
#names of QTL columns
testedSnpPairsQtls = 
  w('isQTL isDirectionalQTL StrandSwitch Redistribution isTpSpecific RNA_concordant RNA_nonconcordant')


uMotifs = c(geneSnpMapMot%>%colnames)%>%setminus(c(w('name strand'),testedSnpPairsQtls,unlist(modelvarlist)))


################################################################################
##########Run the regressions
################################################################################
require(speedglm)

motmapEnrichlist = sapply(simplify=FALSE,testedSnpPairsQtls,function(qtlcol){
        # geneSnpMapMot = geneSnpMapMot[geneSnpMapMot[['set']]==qset ,]        
        mclapply(uMotifs,function(feature){
              # sapply(simplify=FALSE,compartmentstorun,function(compartment){
      sapply(simplify=FALSE,modelvarlist,function(modalvars){
        
          expect_true(qtlcol%in%colnames(geneSnpMapMot))

          #past together the non feature terms
          otherterms = paste0(modalvars,collapse='+')

          expectAll(modalvars,.%>%is_in(colnames(geneSnpMapMot)))

          #and now the full formula
          modelform = as.formula(paste0(qtlcol,' ~ ',feature,' + ',otherterms)%>%gsub('\\+$','',.))

          #finally, fit the glm
          # speedglm(data=geneSnpMapMot[1:1e4,],formula = modelform, family=binomial(link=logit))
          fit = failwith(f=speedglm,quiet=TRUE)(data=geneSnpMapMot[,],formula = modelform, family=binomial(link=logit))


          if(is.null(fit)){
            return(NULL)
          }else{  

            cints = cbind(odds.ratio = coef(fit), confint(fit)%>%set_colnames(w('low.ci high.ci')),feature = feature,type=qtlcol,model = otherterms)
          }
          cints
  })
})
})


motmapEnrichlist[[5]]

#format all this into a data frame
motmapEnrichDF=
  motmapEnrichlist%>%
   unlist(rec=FALSE)%>%
  unlist(rec=FALSE)%>%
  lapply(.%>%as.data.frame%>%add_rownames)%>%
  Filter(.%>%nrow%>%gt(0),.)%>%
   {data.table::rbindlist(.)}%>%
   mutate(created = feature%>%str_extract('created|destroyed|changed'))%>%
   mutate(feature = feature %>%str_replace('_(created|destroyed|changed)',''))%>%
  mutate(rowname = rowname%>%str_replace('TRUE',''))%>%
  filter(rowname %in% uMotifs )%>%
  select(-rowname)%>%
  arrange(desc(odds.ratio))%>%
  mutate(low.ci=as.numeric(low.ci))%>%
  mutate(high.ci=as.numeric(high.ci))%>%
  mutate(odds.ratio=as.numeric(odds.ratio))%>%
  mutate( 
    isdepleted = (low.ci<0 & high.ci <0),
    isenriched = (low.ci>0 & high.ci >0),
    issig = isdepleted | isenriched
  )

motmapEnrichDF$feature
motmapEnrichDF$model
motmapEnrichDF%>%filter(issig)
motmapEnrichDF$feature%<>%str_replace('Group','')
#check our df is correct
# expect_true(motmapEnrichDF%>%uniqueWrt(feature,type,model))
# motmapEnrichDF%>%group_by(feature,type,model,created)%>%filter(n()>1)
stopifnot(motmapEnrichDF%>%tbl_df%>%group_by(feature,type,model,created)%>%tally%$%n%>%eq(1))

# motmapEnrichDF%>%writeTable('/g/furlong/Harnett/TSS_CAGE_myfolder/analysis/motenrichdf.cage.txt')
motmapEnrichDF%>%writeTable('/g/furlong/Harnett/TSS_CAGE_myfolder/analysis/motenrichdf.wide.txt')
# motmapEnrichDF=fread('/g/furlong/Harnett/TSS_CAGE_myfolder/analysis/motenrichdf.txt')

