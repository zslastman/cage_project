Sys.sleep(0.000000)
options(BatchJobs.on.slave = TRUE, BatchJobs.resources.path = '/g/furlong/Harnett/TSS_CAGE_myfolder/feature_analysis/file1255b62b8714e-files/resources/resources_1442334133.RData')
library(checkmate)
library(BatchJobs)
res = BatchJobs:::doJob(
	reg = loadRegistry('/g/furlong/Harnett/TSS_CAGE_myfolder/feature_analysis/file1255b62b8714e-files'),
	ids = c(14L),
	multiple.result.files = FALSE,
	disable.mail = FALSE,
	first = 1L,
	last = 100L,
	array.id = NA)
BatchJobs:::setOnSlave(FALSE)