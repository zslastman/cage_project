
qbetabinom.ab <- function (p, size, shape1, shape2){
	# if( (shape1>100 & shape2 > 100) ){
	# 	#if the ab is large just use the binomial approximation
	# 	prob=shape1/(shape1+shape2)
	# 	qbinom(p,size,prob)
	# }
    my.p <- VGAM::dbetabinom.ab(x = 0:size, , size = size, shape1 = shape1,
        shape2 = shape2)
    cs <- cumsum(my.p)
    above <- which(cs > p)[1]
    below <- above - 1
    if (below > 0) {
        return(below + (p - cs[below])/my.p[above])
    }
    else {
        return(p/my.p[1])
    }
}
library(Rcpp)

#my fast density function for beta binomial
cppFunction(' 
	NumericVector dbetabinom_ab(NumericVector p, NumericVector size, NumericVector shape1, NumericVector shape2, bool log=0) {
		int psize = p.size();
		if(size.size() != psize ) Rcpp::stop("Size vector is the wrong length");
		if(shape1.size() != psize ) Rcpp::stop("shape1 vector is the wrong length");
		if(shape2.size() != psize ) Rcpp::stop("shape2 vector is the wrong length");

		NumericVector out ;
		out = lchoose( size , p)+ lbeta(shape1  + p, shape2 + size - p) -lbeta(shape1,shape2);
		if( !log ) out = exp(out);
		return (out);
	}
')


#my fast quantile function for same
cppFunction(' 	NumericVector qbetabinom_ab(NumericVector p, NumericVector size, NumericVector shape1, NumericVector shape2) {
		//do some checks
		int psize = p.size();
		if(size.size() != psize ) Rcpp::stop("Size vector is the wrong length");
		if(shape1.size() != psize ) Rcpp::stop("shape1 vector is the wrong length");
		if(shape2.size() != psize ) Rcpp::stop("shape2 vector is the wrong length");

		NumericVector out(psize,-1.0) ;
		NumericVector lp = log(p);//log probs

		double dens = 0.0;
		double ldens = 0.0;
		double prevdens = 0.0;

		//for each quantile we want
		for(int i = 0; i < psize; ++i) {
			
			//were going to start at zero, walk up towards size, and when we get to
			//to a number greater than or equal to p we save it and break
			dens=0.0;
			ldens=0.0;
			prevdens=0.0;


			for(int x=0; x < size[i]; ++x){
				//get the log density

				ldens = Rf_lchoose(size[i], x) + Rf_lbeta(shape1[i] +
			    		x, shape2[i] + size[i] - x) - Rf_lbeta(shape1[i],
			      		shape2[i]);
				
				ldens = exp(ldens);



				prevdens = dens;
				dens = dens + ldens ;
				
		
				//if we got there 

				if( dens > p[i] ){
					
					out[i] = (x)+((p[i] -  prevdens) / ldens ) ;
					break;
				}
			}

			if( out[i] == -1.0 ){
				out[i]= size[i];
			}
		}
		return (out);
	}
')

	stopifnot(qbetabinom_ab(c(0.555),3,30,100)==qbetabinom.ab(c(0.555),3,30,100))
	stopifnot(qbetabinom_ab(c(0.555),1000,30,100)==qbetabinom.ab(c(0.555),1000,30,100))
	stopifnot(c(qbetabinom_ab(c(0.1),3,30,100),	qbetabinom_ab(c(0.3),3,31,101))==	qbetabinom_ab(c(0.1,0.3),c(3,3),30:31,100:101))


# gr=normregions
# weight='cutoff'

maxcovDisjoin<-function(gr,weight,sepstrands=FALSE){
	ovinds = gr%>%countOverlaps(gr)>1
	
	ovgrs = gr[	ovinds,NULL]
	ovgrs$weightcol = mcols(gr)[[weight]][ovinds]

	ovgrs = gr%>%setstrand('*')%>%disjoin%>%mergeByOverlaps(ovgrs[,'weightcol'])%>%mergeGR2DT%>%
		group_by(seqnames,start,end)%>%
		slice(which.max(weightcol))%>%
		select(-matches('\\.1$'))%>%
		DT2GR

	mcols(ovgrs)[[weight]]=ovgrs$weightcol

	if(sepstrands) stop('not implemented yet')

	c(gr[!ovinds,weight],ovgrs[,weight])%>%
		coverage(weight=weight)

}



#This function, rather than using RNAseq to infer bin probabilites, will instead infer bin probabilities
#from the cage over a transcript. Lambda becomes the ratio of tss expression to gene noise.
#
#For now I think we can just pass on the gene lengths, rather than transcripts.
#We can total the reads all over teh gene to begin with
#first check how often the reads are concentrated at the beginning
#We should have a couple of examples on hand to see how well this is working
#we should also have a plotting function working


#will return the noise tags only
getNoiseFilt = function(cagetagsnorm,normregions,MINTHRESH,lambda=NULL,NOISELIM=1e-3,repnum=3){
	require(VGAM)
	#check transcripts
	stopifnot( (cagetagsnorm %>% is('GenomicRanges') ))
	stopifnot( (normregions %>% is('GenomicRanges') ))
	stopifnot( (normregions %>% length%>%gt(0) ))
	stopifnot( (cagetagsnorm %>% length%>%gt(0) ))
	cagetagsnormsrl = cagetagsnorm %>% gr2srl
	#only those regions with a decent tag threshold on them
	hasDecentPeak = normregions%>%getStrandedExprMat(cagetagsnormsrl%>%lapply(.%>%gt(MINTHRESH)))%>%apply(1,sum)%>%gt(0)
	normregions=normregions[hasDecentPeak]
	if(length(normregions==0)){
		warning('none of the peaks have asomething over the threshold ')
		return(NULL)
	}
	#don't consider those tags which can't be noise
	cagetagsnorm%<>%subsetByOverlaps(normregions,ignore.strand=TRUE)
	#this could be a lot more optimized....
	#get fake RNAseq data
	ov = findOverlaps(cagetagsnorm,normregions,ignore.strand=TRUE)%>%as.data.frame%>%tbl_df
	normregions$total = normregions%>%getStrandedExprMat(cagetagsnormsrl)%>%apply(1,sum)
	normregions%<>%subset(!is.na(total))
	#try a bunch of different lambda vlues to find the local minimum
	if(is.null(lambda)){
		errorfunc=function(x,pred,score){
			sum(sum(abs((x*pred) - score)))
		}
		#just minimize ambda properly
		message('using error function to get initial lambda')
		regionlambdas = 
			mcols(cagetagsnorm)[ov$queryHits,w('score'),drop=FALSE]%>%
			as.data.frame%>%tbl_df%>%
			mutate(region = ov[['subjectHits']] ) %>%
			mutate(unifpred = (normregions$total/width(normregions))[region] ) %>%
			group_by(region)%>%
			do( nlminb(start=0.01,object=errorfunc,score=.$score,pred=.$unifpred,upper=1,lower=0)$par%>%data.frame(lambda=.))
		rm(ov)
		stopifnot(regionlambdas$region==seq_along(normregions))
		#attribute lambdas to the regions...
		normregions$lambda=regionlambdas$lambda
		#we are faced with most lambdas being close to zero. We can ignoresuch cases
	}
	lambdasample = sample(seq_along(normregions),10,replace=TRUE)
	for(i in 1:repnum){
		# normregions%<>%subsetByOverlaps(vvlgr)
		message('setting lambdas... ' )
		#initial estimates of the amount of noise
		Nlambdas = floor(normregions$total*normregions$lambda)
		uniflevels = Nlambdas / width(normregions)
		rnum = length(normregions)
		#calcuate th cutoffs
		normregions$cutoff = qbetabinom_ab(p=rep(1-1e-4,rnum),size=Nlambdas,shape1=(1+Nlambdas/width(normregions)),1+Nlambdas-(Nlambdas/width(normregions)))
		#srl describing the cutoff regions
		#now set the filter to the maximum filter for whatever gene...
		cutoffsrl = normregions%>%maxcovDisjoin('cutoff')
		#recalculate lambda with the noise tags removed		
		normregions$lambda = 
			getStrandedExprMat(normregions,
				list(
					pos = cagetagsnormsrl$pos*(cagetagsnormsrl$pos < cutoffsrl ),
					neg = cagetagsnormsrl$neg*(cagetagsnormsrl$neg < cutoffsrl )
			))%>%apply(1,sum)
		normregions$lambda = normregions$lambda/normregions$total
		message(paste(normregions$lambda[lambdasample],collpase=','))
	}
	return(list(filter = cutoffsrl,regions = normregions[,w('lambda total')]))
}





nn = function(str){str%>%str_split('\\s+')%>%unlist%>%as.numeric}
#This function returns a grange object of clusters when handed a SimpleRleList
#of tags. It ignores tag height, accept that it eliminates clusters whose maximum
#is lower than the given cutoff. Needs work...
dcluster_cagetags<-function(tags){
  stopifnot(is(tags,'GenomicRanges'))
  if(length(tags)==0){return(tags)}
  require(cluster)
  #we can just ignore anything lower than a certain threshold  
  blocks.gr<-resize(tags,width=301,fix='center')
  blocks.gr<-reduce(blocks.gr)
  #
  ov<-findOverlaps(blocks.gr,tags)
  #
 splitstarts=start(tags)[ov@subjectHits]%>%split(ov@queryHits)
 
 sites = splitstarts[[splitstarts%>%vapply(length,666)%>%which.max]]

 # clusts = tapply(start(tags)[ov@subjectHits],ov@queryHits,simplify=FALSE,function(sites){
 clusts = mclapply( splitstarts,function(sites){
    #for small clusters we needn't do any work
    if(  (max(sites)-min(sites))<300 ) {return(rep(1,length(sites)))}
    #
      #else do hierarchical clustering
      clusts<-cutree(as.hclust(diana(dist(sites))),h=300)
  })
 #now join the group and in group cluster of each to get unique cluster names
 #and assign that as a metadata column in our GR
tags$cluster = clusts%>%list2tab%>%mutate(clust = paste0(name,'.',val))%$%clust
 #return
 tags
}

mergeclusters = function(clusttags,MERGEDIST){
	if(length(clusttags)==0){return(NULL)}
	expclusters = 
		clusttags%>%
		resize(width(.)+MERGEDIST,'center')%>%
		reduce
	#label these
	expclusters$expid = seq_along(expclusters)
	#now merge the original ones by their overlap with these expanded, merged clusters
	clusttags$expclust = clusttags%>%findOverlaps(expclusters)%>%subjectHits
	clusttags%>%
		GR2DT%>%
		group_by(expclust)%>%
		summarise(
			seqnames=unique(seqnames), start=min(start),  end=max(end),
			strand=unique(strand),maxscore=max(score),score=sum(score)
		)%>%DT2GR
}


#get the tags, with clustering info, and do the final processing

getMergedClusters <- function(allclusttags,MINFILT,MERGEDIST,tagfiltN,isolateddist){
	stopifnot(seqlengths(allclusttags)==seqlengths(si))
	#filter out tags of less than N
	allclusttags=allclusttags%>% width1gr%>%subset(score > MINFILT)

	#run the clustering algorithm on them
	allclusttags %<>% dcluster_cagetags
	# #check actin peak is there
	# allclusttags%>%subsetByOverlaps(testwind)%>%{.[which.max(.$score)]}%>%start%>%eq(5794900)
	#for each tag we should calculate the 
	#we want to get rid of instances where we have n or fewer tags,unclustered,isolated by 100bp
	allclusttags$neardist = allclusttags%>%distanceToNearest%$%distance
	singleclusts = allclusttags$cluster%>%table%>%vfilt(.%>%eq(1))%>%names
	allclusttags$singleclust = allclusttags$cluster %in% singleclusts
	#eliminate isolated, single cluster tags
	allclusttags%<>%subset(!(singleclust & (score < tagfiltN) & (neardist<isolateddist)))
	#merge those in the same cluster
	allclusttagsmerge=
		allclusttags%>%GR2DT%>%group_by(strand,cluster)%>%
		summarise(
				seqnames=unique(seqnames), 
				start=min(start),  
				end=max(end),
				strand=unique(strand),
				score=sum(score),
				maxscore=max(score)
		)%>%DT2GR
		#
	allclusttagsmerge%<>%split(.,strand(allclusttagsmerge))
	#now merge the nearby clusters
	allclusttagsmerge =	allclusttagsmerge[-3]%>%lapply(mergeclusters,MERGEDIST)%>%compact
	allclusttagsmerge%>%GRangesList%>%unlist
}


